﻿using System.Net;
using System.Net.Sockets;
// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
             ProtocolType.Udp);

IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 4567);
s.Bind(ipep);

IPAddress ip = IPAddress.Parse("224.5.6.7");

s.SetSocketOption(SocketOptionLevel.IP,
    SocketOptionName.AddMembership,
        new MulticastOption(ip, IPAddress.Any));

byte[] b = new byte[1024];
s.Receive(b);
string str = System.Text.Encoding.ASCII.GetString(b, 0, b.Length);
Console.WriteLine(str.Trim());